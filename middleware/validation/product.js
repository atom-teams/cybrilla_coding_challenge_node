const modelvalidate = require('../../helpers/validate');

module.exports = {
    
    addProduct: function(req, res, next) {
        const validationRule = {
            "name": "required|string",
            "price": "required|hex|min:1"
        }
        modelvalidate(req, res, validationRule, {}, next)
    }

}