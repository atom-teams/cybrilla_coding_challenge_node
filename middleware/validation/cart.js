const modelvalidate = require('../../helpers/validate');

module.exports = {
    
    addCart: function(req, res, next) {
        const validationRule = {
            "product_id": "required|string",
            "quantity": "required|hex|min:1"
        }
        modelvalidate(req, res, validationRule, {}, next)
    }

}