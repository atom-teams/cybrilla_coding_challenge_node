const response = require('./../../helpers/response');
const resMessage = require('./../../helpers/message')
const Validator = require('validatorjs');

module.exports = {
    
    addPromotion: function(req, res, next) {

        let validation = new Validator( req.body , {
            name: ['required', 'string'],
            description: ['required', 'string'],
            p_type: ['required', { 'in': [1, 2] }],
            p_value: ['required', 'hex', 'min:1'],
            d_type: ['required', { 'in': [1, 2] }],
            d_value: ['required', 'hex', 'min:1'],
            product_id: [{ required_if: ['p_type', 1] }]
        });

        if (validation.fails()) {
            response.validationError(
                res,
                resMessage.validationFailed,
                validation.errors,
            );
        } else {
            next();
        }
    }

}