const response = require('./../helpers/response')
const productModel = require('./../models/product');
const resMessage = require('./../helpers/message');

module.exports = {

    addProduct: function (req, res, next) {
        try {
            const data = req.body;
            const newProduct = new productModel({
                name: data.name,
                price: data.price
            });
            newProduct.save(function (err, result) {
                if (err) {
                    response.error(res, err)
                } else {
                    response.success(res, newProduct, resMessage.productAdded);
                }
            })
        } catch(e) {
            response.serverError(res, e.message)
        } 
    },

    getProducts: function (req, res, next) {
        try {
            const query = productModel.find();
            query.lean().exec(function (err, result) {
                if (err) {
                    response.serverError(res, err)
                } else {
                    response.success(res, result, resMessage.productGetAll)
                }
            });
        } catch(e) {
            response.serverError(res, e.message)
        }
    },
};