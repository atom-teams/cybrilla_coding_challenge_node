const response = require('./../helpers/response')
const cartModel = require('./../models/cart');
const productModel = require('./../models/product');
const promotionModel = require('./../models/promotion');
const resMessage = require('./../helpers/message');
const mongoose = require('mongoose');

module.exports = {
    addOrUpdateCart: function (req, res, next) {
        try {
            const data = req.body;
            const newCart = new cartModel({
                product_id: data.product_id,
                quantity: data.quantity
            });
            const query = productModel.findOne({ 
                _id: mongoose.Types.ObjectId(data.product_id)
            });
            query.lean().exec(function (err, result) {
                if (err) {
                    response.serverError(res, err)
                } else {
                    if (result) {
                        const cartQuery = cartModel.findOne({ 
                            product_id: mongoose.Types.ObjectId(data.product_id)
                        });
                        cartQuery.lean().exec(function (cartErr, cartResult) {
                            if (cartErr) {
                                response.serverError(res, cartErr)
                            } else {
                                if (cartResult) {
                                    const cartUpdateQuery = cartModel.updateOne(
                                        { product_id: mongoose.Types.ObjectId(data.product_id) },
                                        { quantity: data.quantity }
                                    );
                                    cartUpdateQuery.lean().exec(function (cartUpdateErr, cartUpdateResult) {
                                        if (cartUpdateErr) {
                                            response.serverError(res, cartUpdateErr)
                                        } else {
                                            response.success(res, cartUpdateResult, resMessage.cartUpdated);
                                        }
                                    });
                                } else {
                                    newCart.save(function (err, result) {
                                        if (err) {
                                            response.error(res, err)
                                        } else {
                                            response.success(res, newCart, resMessage.cartAdded);
                                        }
                                    })
                                }
                            }
                        });
                    } else {
                        response.error(res, resMessage.validProdcutId)
                    }
                }
            });
        } catch(e) {
            response.serverError(res, e.message)
        }
    },

    getProductsFromCart: function (req, res, next) {
        try {

            cartModel.aggregate([
                {
                    $project: {
                        _id: 1,
                        product_id: 1,
                        quantity: 1
                    }
                },
                {
                    $lookup: {
                        from: "promotions",
                        let: { added_qty: "$quantity", prod_id: "$product_id" },
                        pipeline: [
                            {
                                $match: {
                                    is_active: true,
                                    $expr: {
                                        $and: [
                                            {
                                                $eq: [ "$product_id",  "$$prod_id" ]
                                            },
                                            {
                                                $eq: [ "$p_type", 1 ]
                                            },
                                            {
                                                $or: [
                                                    { $eq: [ "$p_value",  "$$added_qty" ] },
                                                    { $lt: [ "$p_value", "$$added_qty" ] }
                                                ]
                                            }
                                        ]
                                    }
                                }
                            },
                            { 
                                $sort : { 
                                    p_value : -1
                                } 
                            }
                        ],
                        as: "validPromos"
                    }
                },
                {
                    $lookup: {
                        from: "products",
                        localField: "product_id",
                        foreignField: "_id",
                        as: "prodDetail"
                     }
                },
                {
                    $unwind: '$prodDetail'
                },
            ], function (err, result) {
                if (err) {
                    response.serverError(res, err)
                } else {

                    var cartSubtotal = 0;
                    var cartDiscount = 0;
                    var cartGrandTotal = 0;

                    // Check Multiby Promotions
                    result.map(function(data, index) {
                        let wantedQuanity = data.quantity;
                        var balanceQuanity = data.quantity;
                        var subTotal = 0;
                        var balanceSubTotal = 0;
                        var discountTotal = 0;
                        var productPrice = parseFloat(data.prodDetail.price)
                        var appliedPromotions = [];

                        // Subtotal Calculate
                        subTotal = parseFloat(productPrice * wantedQuanity);
                        balanceSubTotal = subTotal

                        // Discount Calculate
                        if (data.validPromos && data.validPromos.length > 0) {
                            data.validPromos.forEach(promos => {
                                if (balanceQuanity >= parseFloat(promos.p_value)) {

                                    var mutiple = parseInt(balanceQuanity / parseFloat(promos.p_value))
                                    balanceQuanity = parseInt(balanceQuanity % parseFloat(promos.p_value))
                                    switch (promos.d_type) {
                                        case 1:
                                            discountTotal += ((parseFloat(productPrice * parseFloat(promos.p_value)) / 100) * parseFloat(promos.d_value));
                                        break;
                                        case 2:
                                            discountTotal += parseFloat(promos.d_value);
                                        break;
                                        default:
                                            break;
                                    }

                                    discountTotal = parseFloat(discountTotal * mutiple);

                                    appliedPromotions.push(promos)
                                }
                            });
                        }

                        data.validPromos = appliedPromotions;
                        data.sub_total = subTotal;
                        data.discount_total = discountTotal;
                        data.total = parseFloat(parseFloat(subTotal) - parseFloat(discountTotal));

                    });

                    // Total Calculation
                    result.forEach(element => {
                        cartSubtotal += element.total
                        cartGrandTotal += element.total
                    });

                    let modifiedResponse = {
                        list: result,
                        cart: {
                            sub_total: cartSubtotal,
                            discount_total: cartDiscount,
                            grand_total: cartGrandTotal
                        }
                    }

                    // Check Total basket Promotions
                    const basketQuery = promotionModel.find({
                        p_type: 2,
                        is_active: true,
                        $expr: {
                             $or: [
                                 {
                                    $eq: [ "$p_value", cartGrandTotal ]
                                 },
                                 {
                                    $lt: [ "$p_value", cartGrandTotal ]
                                 }
                             ]
                        },
                    }).sort( { p_value: -1 } );

                    basketQuery.lean().exec(function (basketErr, basketResult) {
                        if (basketErr) {
                            response.serverError(res, basketErr)
                        } else {
                            if (basketResult) {

                                var tempTotal = cartGrandTotal;
                                var basketPromotion = [];
                                var discountTotal = 0;

                                basketResult.forEach(promos => {
                                    if (tempTotal >= parseFloat(promos.p_value)) {
                                        switch (promos.d_type) {
                                            case 1:
                                                let calDiscount = ((parseFloat(tempTotal) / 100) * parseFloat(promos.d_value)); 
                                                discountTotal += calDiscount
                                                tempTotal = tempTotal - parseFloat(calDiscount);
                                            break;
                                            case 2:
                                                discountTotal += parseFloat(promos.d_value);
                                                tempTotal = tempTotal - parseFloat(promos.d_value);
                                            break;
                                            default:
                                                break;
                                        }
                                        basketPromotion.push(promos)
                                    }
                                });

                                modifiedResponse.basketPromotions = basketPromotion;

                                modifiedResponse.cart.discount_total = (modifiedResponse.cart.discount_total + discountTotal);
                                modifiedResponse.cart.grand_total = (modifiedResponse.cart.grand_total - discountTotal)

                                response.success(res, modifiedResponse, resMessage.cartGetAll)
                            } else {
                                response.success(res, modifiedResponse, resMessage.cartGetAll)
                            }
                        }
                    });
                }
            });
        } catch(e) {
            response.serverError(res, e.message)
        }
    },

    deleteProductsFromCart: function (req, res, next) {
        try {
            const cartQuery = cartModel.findOne({ 
                _id: mongoose.Types.ObjectId(req.params.id)
            });
            cartQuery.lean().exec(function (cartErr, cartResult) {
                if (cartErr) {
                    response.serverError(res, cartErr)
                } else {
                    if (cartResult) {
                        const deleteQuery = cartModel.remove({
                            _id: mongoose.Types.ObjectId(req.params.id)
                        },{
                            justOne: true
                        });
                        deleteQuery.lean().exec(function (cartDeleteErr, cartDeleteResult) {
                            if (cartDeleteErr) {
                                response.serverError(res, cartDeleteErr)
                            } else {
                                response.success(res, cartDeleteResult, resMessage.cartDeleted)
                            }
                        });
                    } else {
                        response.error(res, resMessage.validCartId)
                    }
                }
            });
        } catch(e) {
            response.serverError(res, e.message)
        }
    }
};