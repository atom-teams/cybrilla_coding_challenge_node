const response = require('./../helpers/response')
const promotionModel = require('./../models/promotion');
const productModel = require('./../models/product');
const resMessage = require('./../helpers/message');
const mongoose = require('mongoose');

module.exports = {
    addPromotion: function (req, res, next) {
        try {
            const data = req.body;
            var customErrMsg = {};

            // Custom Validation
            if (parseInt(data.d_type) == 1) {
                if (parseInt(data.d_value) < 1 || parseInt(data.d_value) > 100) {
                    customErrMsg.d_type = ["The d_value must be greater than 0 and less than 100 when d_type 1"];
                }
            }

            if (Object.keys(customErrMsg).length > 0) {
                response.validationError(res, "Validation error", {
                    errors: customErrMsg
                })
                return
            }

            const newPromotion = new promotionModel({
                name: data.name,
                description: data.description,
                p_type: data.p_type,
                p_value: data.p_value,
                d_type: data.d_type,
                d_value: data.d_value,
                product_id: data.product_id
            });

            if (data.p_type == 1) {
                const query = productModel.findOne({ 
                    _id: mongoose.Types.ObjectId(data.product_id)
                });
                query.lean().exec(function (err, result) {
                    if (err) {
                        response.serverError(res, err)
                    } else {
                        if (result) {
                            newPromotion.save(function (err, result) {
                                if (err) {
                                    response.error(res, err)
                                } else {
                                    response.success(res, newPromotion, resMessage.promotionAdded);
                                }
                            })
                        } else {
                            response.error(res, resMessage.validProdcutId)
                        }
                    }
                });
            } else {
                newPromotion.save(function (err, result) {
                    if (err) {
                        response.error(res, err)
                    } else {
                        response.success(res, newPromotion, resMessage.promotionAdded);
                    }
                })
            }
            
        } catch(e) {
            response.serverError(res, e.message)
        } 
    },
    getPromotions: function (req, res, next) {
        try {
            const query = promotionModel.find();
            query.lean().exec(function (err, result) {
                if (err) {
                    response.serverError(res, err)
                } else {
                    response.success(res, result, resMessage.promotionGetAll)
                }
            });
        } catch(e) {
            response.serverError(res, e.message)
        }
    },
    deletePromotion: function (req, res, next) {
    },
};