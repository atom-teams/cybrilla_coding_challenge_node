const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const SchemaTypes = mongoose.Schema.Types;

const activity = {
    createdAt: { type: Date, default: Date.now },
    modifiedAt: { type: Date, default: null }
};

const productSchema = new Schema({
    name: { type: String, trim: true },
    price: { type: SchemaTypes.Decimal128, trim: true },
    activity: activity
});

var productModel = module.exports = mongoose.model('Products', productSchema);
module.exports = productModel;