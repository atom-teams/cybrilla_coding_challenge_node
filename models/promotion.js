const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const SchemaTypes = mongoose.Schema.Types;

const activity = {
    createdAt: { type: Date, default: Date.now },
    modifiedAt: { type: Date, default: null }
};

const promotionSchema = new Schema({
    name: { type: String, trim: true },
    description: { type: String, trim: true },
    /**
     * Promotion Types (p_type)
     * 1 = Multi Buy
     * 2 = Basket Total Discount
     */
    p_type: { type: Number, trim: true }, 
    /**
     * Promotion Value (p_value)
     * If you choose promotion type is (Multi Buy) it act as quantity
     * If you choose promotion type is (Basket Total Discount) it act as price
     */
    p_value: { type: SchemaTypes.Decimal128, trim: true },
    product_id: { type: SchemaTypes.ObjectId, ref: 'Products' },
    /**
     * Discount type (d_type)
     * 1 = Percentage(%)
     * 2 = Price
     */
    d_type: { type: Number, trim: true },
    /**
     * Discount Value (d_value)
     * If you choose discount type is (Percentage) it act as percentage
     * If you choose discount type is (Price) it act as price
     */
    d_value: { type: SchemaTypes.Decimal128, trim: true },
    is_active: { type: SchemaTypes.Boolean, default: true },
    activity: activity
});

const promotionModel = module.exports = mongoose.model('Promotions', promotionSchema);
module.exports = promotionModel;