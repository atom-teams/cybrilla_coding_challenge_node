const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const SchemaTypes = mongoose.Schema.Types;

const activity = {
    createdAt: { type: Date, default: Date.now },
    modifiedAt: { type: Date, default: null }
};

const cartSchema = new Schema({
    product_id: { type: SchemaTypes.ObjectId, ref: 'Products' },
    quantity: { type: Number, trim: true },
    activity: activity
});

var cartModel = module.exports = mongoose.model('cart', cartSchema);
module.exports = cartModel;