
// Importing controllers
const productCon = require('./../controllers/product');
const promotionCon = require('./../controllers/promotion');
const cartCon = require('./../controllers/cart');

// Importing validation files
const productValid = require('../middleware/validation/product');
const promotionValid = require('../middleware/validation/promotion');
const cartValid = require('../middleware/validation/cart');

module.exports = function (app) {

    // Root Path
    app.get('/', function (req, res, next) {
        return res.send("WELCOME TO REST API");
    });

    // Products Path
    app.post({path: '/api/v1/product/add'}, productValid.addProduct, productCon.addProduct);
    app.get({path: '/api/v1/products'}, productCon.getProducts);

    // Promotions Path
    app.post({path: '/api/v1/promotion/add'}, promotionValid.addPromotion, promotionCon.addPromotion);
    app.get({path: '/api/v1/promotions'}, promotionCon.getPromotions);

    // Cart
    app.post({path: '/api/v1/cart/add'}, cartValid.addCart, cartCon.addOrUpdateCart);
    app.put({path: '/api/v1/cart/update'}, cartValid.addCart, cartCon.addOrUpdateCart);    
    app.get({path: '/api/v1/cart'}, cartCon.getProductsFromCart);
    app.del({path: '/api/v1/cart/delete/:id'}, cartCon.deleteProductsFromCart);

};