module.exports = {

    // Common
    validationFailed: "Validation failed",
    somethingWentWrong: "!Oops something went wrong",
    
    // Products
    productAdded: "The product has been added successfully",
    validProdcutId: "Please provide valid product id",
    productGetAll: "The products has been successfully fetched",

    // Promotions
    promotionAdded: "The promotion has been added successfully",
    promotionGetAll: "The promotions has been successfully fetched",

    // Cart
    cartAdded: "The cart product has been added successfully",
    cartUpdated: "The cart product has been updated successfully",
    cartGetAll: "The cart items has been successfully fetched",
    cartDeleted: "The cart items has been successfully deleted",
    validCartId: "Please provide valid cart id",

}