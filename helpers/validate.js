const Validator = require('validatorjs');
const response = require('./response');
const resMessage = require('./message')

const validator = (body, rules, customMessages, callback) => {
    const validation = new Validator(body, rules, customMessages);
    validation.passes(() => callback(null, true));
    validation.fails(() => callback(validation.errors, false));
};

const modelvalidate = (req, res, rules, customMessages, next) => {
    validator(req.body, rules, customMessages, (err, status) => {
        if (!status) {
            response.validationError(
                res,
                resMessage.validationFailed,
                err,
            );
        } else {
            next();
        }
    });
}

module.exports = modelvalidate;