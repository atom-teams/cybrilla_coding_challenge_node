const resMessage = require('./message');

module.exports = {

    success: function(res, data, message) {
        return res.json(200, {
            status: true,
            status_code: 200,
            message: message,
            data: data
        });
    },

    error: function(res, message) {
        return res.json(400, {
            status: false,
            status_code: 400,
            message: message
        });
    },

    serverError: function(res, message) {
        return res.json(500, {
            status: false,
            status_code: 500,
            message: message ? message : resMessage.somethingWentWrong
        });
    },

    validationError: function(res, message, data) {
        return res.json(402, {
            status: false,
            status_code: 402,
            message: message,
            data: data
        });
    }

}