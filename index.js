const restify = require('restify');
const mongoose = require('mongoose');
const config = require('./config/env_config');
const corsMiddleware = require('restify-cors-middleware')

var app = restify.createServer();

app.use(restify.plugins.queryParser());  
app.use(restify.plugins.bodyParser());
const cors = corsMiddleware({
	preflightMaxAge: 5, //Optional
	origins: ['*'],
	allowHeaders: [],
	exposeHeaders: []
})
  
app.pre(cors.preflight)
app.use(cors.actual)

app.opts("/.*/", function (req, res, next) {
	res.header("Access-Control-Allow-Origin", '*');
	res.header("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE, PATCH, OPTIONS');
	res.header('Access-Control-Expose-Headers', 'Authorization');
	res.header("Access-Control-Allow-Headers", 'Content-type, token, type, user, Authorization, Content-Type, content-type, X-XSRF-TOKEN');
	res.header("Access-Control-Allow-Credentials", 'true');
	if (req.method == 'OPTIONS') {
		res.send(200);
	} else {
		return next();
	}

});

// Connect database
var options = { useNewUrlParser: true };
mongoose.connect(config.DB_CONNECTION_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
	auth: {
	  user: config.DB_USERNAME,
	  password: config.DB_PASSWORD
	}
})
var db = mongoose.connection;

db.on('error', function (error) {
	console.log(error);
	console.log('Error occurred from db');
});

db.once('open', function dbOpen() {
	console.log('Successfully opened the db');
});

app.listen(config.PORT, function () {
	console.log('Server listening on port number', config.PORT);
});

app.use(function logger(req, res, next) {
	console.log(new Date(), req.method, req.url);
	return next();
});

//Assign Router
var routes = require('./routes/index')(app);