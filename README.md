Welcome to cybrilla coding challenge nodejs

Promotions:

- Procedure for creating promotions
- There are two types of promotions, One is (Multi Buy) and another one is (Basket Total Discount). Based on this we have created Model and API.

Promotion Model
    => name 
        - "Please enter the promo name",
    => description 
        - "Please enter the promo description"
    => p_type 
        - It is a promotion type(p_type). Currently only two types of promotions available.
        => Multi Buy 
            - If you want to create (Multi Buy) promotion, just fill the number 1
        => Basket Total Discount 
            - If you want to create (Basket Total Discount) promotion, just fill the number 2
    => p_value 
        - It is a promotion type(p_value). If you choose promotion type is (Multi Buy) it act as quantity or if you choose promotion type is (Basket Total Discount) it act as price
    => d_type 
        - It is a promotion type(d_type). Currently two types of discount type is available.
        => Percentage 
            - If you want to discount by (Percentage), just fill the number 1
        => Price
            - If you want to discount by (price), just fill the number 2
    => d_value
        - - It is a discount type(d_value). If you choose discount type is (Percentage) it act as percentage or If you choose discount type is (Price) it act as price
    => product_id: If choose the (Multi Buy), this field is required one.


Example for create promotions:

    => If 3 of Item A is purchased, the price of all three is Rs 75(i.e multiples of 3 discount)
    {
        "name": "3 of Item A is purchased, the price of all three is Rs 75",
        "description": "3 of Item A is purchased, the price of all three is Rs 75",
        "p_type": 1,
        "p_value": 3,
        "d_type": 2,
        "d_value": 15,
        "product_id": "600eb330ce121a7389af2e56"
    }
    => if 2 of Item B is purchased, the price of both is Rs 35(i.e multiples of 2 discount)
    {
        "name": "2 of Item B is purchased, the price of both is Rs 35",
        "description": "2 of Item B is purchased, the price of both is Rs 35",
        "p_type": 1,
        "p_value": 2,
        "d_type": 2,
        "d_value": 5,
        "product_id": "600eb33cce121a7389af2e57"
    }
    => If the total basket price(after previous discounts) is over Rs 150, the basket receives an
additional discount of Rs 20.
    {
        "name": "total basket price(after previous discounts) is over Rs 100, the basket receives an additional discount of Rs 20.",
        "description": "total basket price(after previous discounts) is over Rs 100, the basket receives an additional discount of Rs 20.",
        "p_type": 2,
        "p_value": 150,
        "d_type": 2,
        "d_value": 20
    }
    => If the total basket price(after previous discounts) is over Rs 150, the basket receives an
additional discount of 20%.
    {
        "name": "total basket price(after previous discounts) is over Rs 100, the basket receives an additional discount of Rs 20.",
        "description": "total basket price(after previous discounts) is over Rs 100, the basket receives an additional discount of Rs 20.",
        "p_type": 2,
        "p_value": 150,
        "d_type": 1,
        "d_value": 20
    }